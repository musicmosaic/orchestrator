package se.sebastianfernstedt.musicmosaic.orchestrator

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(properties = ["spotify.client-secret=test"])
class OrchestratorApplicationTests {

    @Test
    fun contextLoads() {

        println("Test run")
    }

}
