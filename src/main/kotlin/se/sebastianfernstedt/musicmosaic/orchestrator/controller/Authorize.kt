package se.sebastianfernstedt.musicmosaic.orchestrator.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import se.sebastianfernstedt.musicmosaic.orchestrator.service.AuthorizationService

@Controller
class AuthorizeController {

    @Autowired
    lateinit var authorizationService: AuthorizationService

    @GetMapping("/authorize")
    fun authorize(): String {
        return "redirect:" + authorizationService.startAuthorize()
    }

    @GetMapping(value = ["/authorize/callback"], params = ["code"])
    fun authorizeCallbackSuccess(
            @RequestParam code: String,
            @RequestParam state: String,
            model: Model
    ): String {
        authorizationService.authorize(code = code, state = state)

        return "mosaic"
    }

    @GetMapping(value = ["/authorize/callback"], params = ["error"])
    fun authorizeCallbackFailure(
            @RequestParam error: String,
            @RequestParam state: String
    ): String = "Auth failed."
}