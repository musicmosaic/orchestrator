package se.sebastianfernstedt.musicmosaic.orchestrator.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import se.sebastianfernstedt.musicmosaic.orchestrator.service.ImageService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession




@Controller
class Mosaic {

    @Autowired
    lateinit var imageService: ImageService

    @GetMapping(path = ["/mosaic"], produces = [MediaType.IMAGE_JPEG_VALUE])
    @ResponseBody
    fun getMosaic(request: HttpServletRequest): ByteArray {
        val mosaic = imageService.getMosaic()

        // Kill session, to allow users running the same flow again
        val session: HttpSession = request.getSession(false)
        session.invalidate()

        return mosaic
    }
}