package se.sebastianfernstedt.musicmosaic.orchestrator.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class WelcomeController {

    @GetMapping
    fun welcome(model: Model) = "welcome"
}