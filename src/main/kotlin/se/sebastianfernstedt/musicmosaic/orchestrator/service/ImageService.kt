package se.sebastianfernstedt.musicmosaic.orchestrator.service

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import se.sebastianfernstedt.musicmosaic.orchestrator.data.SessionData
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO


@Service
class ImageService {

    @Autowired
    lateinit var sessionData: SessionData

    fun getMosaic(): ByteArray {

        val widthCount = 5
        val heightCount = 5

        val totalCount = widthCount * heightCount
        val singleImagePixels = 300

        val topTracksUrl = "https://api.spotify.com/v1/me/top/tracks?limit=$totalCount"

        // Get user top tracks
        val restTemplate = RestTemplate()
        val headers = HttpHeaders()
        headers.setBearerAuth(sessionData.accessToken)
        val request = HttpEntity<Void>(headers)

        val response: ResponseEntity<TrackList> = restTemplate.exchange(topTracksUrl, HttpMethod.GET, request,
                TrackList::class.java)

        // Download images
        val images = ArrayList<ByteArray>()
        for (track: Track in response.body!!.items) {
            val url = track.album.images.find { it.height == singleImagePixels.toString() }!!.url
            val imageData = restTemplate.getForEntity(url, ByteArray::class.java)
            images.add(imageData.body!!)
        }

        // Transform images into mosaic
        assert(images.size == totalCount)

        val width = widthCount * singleImagePixels
        val height = heightCount * singleImagePixels

        val combinedImage = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
        val graphics2D = combinedImage.createGraphics()

        images.withIndex().forEach { (index, imageArray) ->
            val xPosition = index.rem(widthCount)
            val yPosition = index / widthCount

            val image = ImageIO.read(ByteArrayInputStream(imageArray))
            graphics2D.drawImage(image, null, xPosition * singleImagePixels, yPosition * singleImagePixels)
        }
        graphics2D.dispose()

        ByteArrayOutputStream().use { out ->
            ImageIO.write(combinedImage, "jpg", out)
            return out.toByteArray()
        }
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class TrackList(var items: List<Track>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Track(var album: Album, var name: String)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Album(var images: List<Image>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Image(var height: String, var url: String, var width: String)