package se.sebastianfernstedt.musicmosaic.orchestrator.service

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import se.sebastianfernstedt.musicmosaic.orchestrator.data.SessionData
import java.util.*


@Service
class AuthorizationService {
    private val logger = LogFactory.getLog(AuthorizationService::class.java)
    
    @Autowired
    lateinit var sessionData: SessionData

    @Value("\${spotify.authorize-base-url}")
    lateinit var authorizeStartUrl: String

    @Value("\${spotify.client-id}")
    lateinit var clientId: String

    @Value("\${spotify.client-secret}")
    lateinit var clientSecret: String

    @Value("\${spotify.redirect-url}")
    lateinit var redirectUrl: String

    val authorizeFinishUrl = "https://accounts.spotify.com/api/token"

    fun startAuthorize(): String {
        sessionData.state = UUID.randomUUID().toString()

        return getAuthorizeUrl(sessionData.state)
    }

    fun authorize(code: String, state: String) {

        // Assert that received state is same as saved in session
        assert(state != sessionData.state) {"State does not match saved state."}

        // Create request
        val restTemplate = RestTemplate()

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_FORM_URLENCODED
        headers.setBasicAuth(clientId, clientSecret)

        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("code", code)
        map.add("grant_type", "authorization_code")
        map.add("redirect_uri", redirectUrl)

        val entity = HttpEntity(map, headers)

        // Exchange request for response
        val response: ResponseEntity<AuthorizeResponse> = try {
            restTemplate.exchange(authorizeFinishUrl, HttpMethod.POST, entity,
                    AuthorizeResponse::class.java)
        } catch (t: Throwable) {
            logger.info(t.localizedMessage)
            throw t
        }

        // Check that response is OK
        assert(response.statusCodeValue == 200)

        // Set access token on session
        sessionData.accessToken = response.body!!.access_token
    }

    private fun getAuthorizeUrl(state: String): String {
        return UriComponentsBuilder.fromHttpUrl(authorizeStartUrl)
                .queryParam("client_id", clientId)
                .queryParam("response_type", "code")
                .queryParam("redirect_uri", redirectUrl)
                .queryParam("scope", "user-top-read")
                .queryParam("state", state)
                .queryParam("show_dialog", "true")
                .toUriString()
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class AuthorizeResponse(var access_token: String, var expires_in: Int)