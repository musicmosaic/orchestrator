package se.sebastianfernstedt.musicmosaic.orchestrator.data

import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import org.springframework.web.context.WebApplicationContext
import java.lang.AssertionError

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
class SessionData {
    private var _state: String? = null
    var state: String
        get() {
            return _state ?: throw AssertionError("State has not been set")
        }
        set(value) {
            if (_state != null) {
                throw AssertionError("State has already been set")
            }
            _state = value
        }

    private var _accessToken: String? = null
    var accessToken: String
        get() {
            return _accessToken ?: throw AssertionError("AccessToken has not been set")
        }
        set(value) {
            if (_accessToken != null) {
                throw AssertionError("AccessToken has already been set")
            }
            _accessToken = value
        }
}