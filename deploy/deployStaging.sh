#!/bin/bash

# any future command that fails will exit the script
set -e

# Get SSH client
command -v ssh-agent || ( apk --update add openssh-client )
echo "SSH agent installed."

# Add private key for SSH to agent
eval $(ssh-agent -s)
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

# Add known hosts to ssh to avoid ssh key verification check
echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Split list of servers into array
IFS=';' read -ra SERVER_ARRAY <<< "$DEPLOY_SERVERS"
echo "SERVER_ARRAY ${SERVER_ARRAY}"

# Lets iterate over this array and ssh into each EC2 instance
# Once inside the server, run remoteStartImage.sh
for server in "${SERVER_ARRAY[@]}"
do
  echo "deploying to ${server}"
  ssh ec2-user@${server} "bash -s '$GITLAB_DEPLOY_USER' '$GITLAB_DEPLOY_TOKEN' 'spotify.client-secret=$SPOTIFY_CLIENT_SECRET'" < ./deploy/remoteStartImage.sh
  # TODO: Test changing to using built in CI user?
done