#!/bin/bash

# any future command that fails will exit the script
set -e

GITLAB_DEPLOY_USER=$1
GITLAB_DEPLOY_TOKEN=$2
ENVIRONMENT_VARIABLES=$3

echo "Running remote start image script, deploying as {$GITLAB_DEPLOY_USER}"

# Login to registry
docker login registry.gitlab.com/musicmosaic/orchestrator -u "$GITLAB_DEPLOY_USER" -p "$GITLAB_DEPLOY_TOKEN"

# Pull newest image
docker pull registry.gitlab.com/musicmosaic/orchestrator

# Remove if running - || true helps not return error to script if not present, making script continue
docker rm --force orchestrator || true

# Run new image in detatched mode
docker run --publish 8080:8080 --detach --name orchestrator -e "$ENVIRONMENT_VARIABLES" registry.gitlab.com/musicmosaic/orchestrator

echo "New version has been deployed!"

docker logout registry.gitlab.com/musicmosaic/orchestrator